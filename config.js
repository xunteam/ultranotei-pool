var api = "https://alpha.ultranote.org:8119";

var email = "support@ultranote.org";
var telegram = "https://t.me/joinchat/H0xqnU-vzSObrkQlov38dQ";
var discord = "https://discordapp.com/invite/YbpHVSd";

var marketCurrencies = ["{symbol}-BTC", "{symbol}-USD", "{symbol}-EUR", "{symbol}-CAD"];

var blockchainExplorer = "http://explorer.ultranote.org/?hash={id}#blockchain_block";
var transactionExplorer = "http://explorer.ultranote.org/?hash={id}#blockchain_transaction";

var themeCss = "themes/default.css";
var defaultLang = 'en';
